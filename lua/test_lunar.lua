
local nm_connecting_animation = {
	"nm-stage01-connecting01",
	"nm-stage01-connecting02",
	"nm-stage01-connecting03",
	"nm-stage01-connecting04",
	"nm-stage01-connecting05",
	"nm-stage01-connecting06",
	"nm-stage01-connecting07",
	"nm-stage01-connecting08",
	"nm-stage01-connecting09",
	"nm-stage01-connecting10",
	"nm-stage01-connecting11",
}

local main_window = Window{ label = "test", _signal_destroy = quit }
local btn = StateButton{
	parent = main_window,
	label = "foo bar baz",
	icon = "network-disconnected",
	_looks = {
		active     = { active=true, icon="network-wireless-connected" },
		active_00  = { active=true, icon="network-wireless-connected-00" },
		active_25  = { active=true, icon="network-wireless-connected-25" },
		active_50  = { active=true, icon="network-wireless-connected-50" },
		active_75  = { active=true, icon="network-wireless-connected-75" },
		active_100 = { active=true, icon="network-wireless-connected-100" },
		disconnected = { active=false, icon="network-wireless-disconnected" },
		connecting = { active=true, icon="network-wireless-acquiring", animation=nm_connecting_animation, fps=8},
		active_ap  = { active=true, icon="network-wireless-hotspot"},
		active_no_route = { active=true, icon="network-wireless-no-route"},
	},
	state = "disconnected",
}

function on_stdin_line(line)
	print("Received input from stdin: "..line)
	btn.state = line
end

_global_setting_disable_animation = true
