main_window = Window{
	label = "Keyboard",
	accept_focus = false,
	_signal_destroy = quit,
}

layouts = {}

layouts.letters = {
	default_width = 3,
	{
		{ type_key="Escape", label="Esc" },"q","w","e","r","t","z","u","i","o","p",{ type_key="BackSpace", label="🠸✗", icon="edit-clear-symbolic", width=4 },
	},{
		{ type_key="Tab", width=4, label="⭾" },"a","s","d","f","g","h","j","k","l",{ type_key="apostrophe", label="'" },{ tpye_key="Delete", label="Del" },
	},{
		{ hold_key="Shift", width=5, label="⇧", shift_layout=true },"y", "x", "c", "v", "b", "n", "m", { type_key="comma", label=",", type_shifted="semicolon", shifted_label=";" }, { type_key="period", label=".", type_shifted="colon"; shifted_label=":" }, { type_key="Return", width = 5, label="↲" },
	},{
		{ hold_key="Ctrl", label="Ctrl" }, { hold_key="Super", label="⌘", icon="start-here-symbolic" }, { hold_key="Alt", label="Alt" }, { layout_key="numbers", label="12?" }, { type_key="space", label=" ", width=13 }, { type_key="Left", label="🠔" }, { type_key="Right", label="🠒" }, { type_key="Up", label="🠕" }, { type_key="Down", label="🠗" },
	}
}

layouts.numbers = {
	default_width = 3,
	keyup = {"Shift","Ctrl","Super","Alt"},
	{
		{ type_key="Escape", label="Esc" },"1","2","3","4","5","6","7","8","9","0",{ type_key="BackSpace", label="🠸✗", icon="edit-clear-symbolic", width=4 },
	},{
		{ type_key="Tab", width=4, label="⭾" },"#","%","+","-","*","/","\\","(",")",{ type_key="apostrophe", label="'" },{ tpye_key="Delete", label="Del" },
	},{
		{ hold_key="Shift", width=5, label="⇧", shift_layout=true },"@", "=", "\"", "?", "!", ";", ":", { type_key="comma", label=",", type_shifted="semicolon", shifted_label=";" }, { type_key="period", label=".", type_shifted="colon"; shifted_label=":" }, { type_key="Return", width = 5, label="↲" },
	},{
		{ hold_key="Ctrl", label="Ctrl" }, { hold_key="Super", label="⌘", icon="start-here-symbolic" }, { hold_key="Alt", label="Alt" }, { layout_key="letters", label="abc" }, { type_key="space", label=" ", width=13 }, { type_key="Left", label="🠔" }, { type_key="Right", label="🠒" }, { type_key="Up", label="🠕" }, { type_key="Down", label="🠗" },
	}
}

layouts.sym = {
	default_width = 3,
	keyup = {"Shift","Ctrl","Super","Alt"},
	{
		{ type_key="Escape", label="Esc" },"`","~","_","<","|",">","{","}","[","]",{ type_key="BackSpace", label="🠸✗", icon="edit-clear-symbolic", width=4 },
	},{
		{ type_key="Tab", width=4, label="⭾" },"$","€","¥","₽","£","&","§","^","°","…",{ tpye_key="Delete", label="Del" },
	},{
		{ hold_key="Shift", width=5, label="⇧", shift_layout=true },"„", "”", "«", "»", "¿", "¡", "™", "®", "©", { type_key="Return", width = 5, label="↲" },
	},{
		{ hold_key="Ctrl", label="Ctrl" }, { hold_key="Super", label="⌘", icon="start-here-symbolic" }, { hold_key="Alt", label="Alt" }, { layout_key="numbers", label="12?" }, { type_key="space", label=" ", width=13 }, { type_key="Left", label="🠔" }, { type_key="Right", label="🠒" }, { type_key="Up", label="🠕" }, { type_key="Down", label="🠗" },
	}
}

local function keyboard_create_buttons(this)
	-- create callback functions
	local _this = make_weak_table()
	_this.signal = this.signal
	_this.this = this
	local function on_key_clicked(button)
		if button._key.type_key then
			if _this.signal.type_key then
				if button._key.type_shifted and _this.this._is_shifted then
					_this.signal.type_key(button._key.type_shifted)
				else
					_this.signal.type_key(button._key.type_key)
				end
			end
		elseif button._key.layout_key then
			if _this.signal.set_layout then
				_this.signal.set_layout(button._key.layout_key)
			end
		elseif button._key.command_key then
			if _this.signal.command then
				_this.signal.command(button._key.command_key)
			end
		end
	end
	local function on_key_toggled(button, state)
		if button._key.hold_key then
			if _this.signal.keydown and _this.signal.keyup then
				if state == "true" then
					_this.signal.keydown(button._key.hold_key)
					if button._key.shift_layout then
						_this.this._is_shifted = true
						_this.this:_label_keys()
					end
				else
					_this.signal.keyup(button._key.hold_key)
					if button._key.shift_layout then
						_this.this._is_shifted = false
						_this.this:_label_keys()
					end
				end
			end
		end
	end
	--apply layout
	local ypos = 0
	for y,row in ipairs(this._layout) do
		local xpos = 0
		for x,key in ipairs(row) do
			local button_id = tostring(x).."_"..tostring(y)
			local w = row.default_width or this._layout.default_width or 1
			local h = 1
			local icon = nil
			local _key = key
			local is_toggle = false
			if type(key) == "table" then
				if key.width then w = key.width end
				icon = key.icon
				is_toggle = not not (key.hold_key)
			else
				_key = { type_key=key }
				if #key == 1 then
					_key.type_shifted = key:upper()
				end
			end
			this._buttons[button_id] = ((is_toggle and ToggleButton) or Button){
				parent = this,
				position = ("%d_%d_%d_%d"):format(xpos, ypos, w, h),
				label = ((not icon) and "-" or nil),
				icon = icon,
				_key = _key,
				_signal_clicked = on_key_clicked,
				_signal_toggled = on_key_toggled,
			}
			xpos = xpos+w
		end
		ypos = ypos+1
	end
end

local function keyboard_label_keys(this)
	for y,row in ipairs(this._layout) do	
		for x,key in ipairs(row) do
			local button_id = tostring(x).."_"..tostring(y)
			local button = this._buttons[button_id]
			if button then
				if type(key) == "string" then
					if this._is_shifted and #key == 1 then
						button.label = key:upper()
					else
						button.label = key
					end
				else
					local icon = (this._is_shifted and key.shifted_icon) or key.icon
					if icon then
						button.icon = icon
					else
						local label = key.label or key.type_key or key.hold_key
						if this._is_shifted then
							if key.shifted_label then
								label = key.shifted_label
							elseif #label == 1 then
								label = label:upper()
							end
						end
						button.icon = nil
						button.label = label
					end
				end
			end
		end
	end
end

local function Keyboard(attr)
	local this = Grid(attr)
	this._buttons = {}
	this._label_keys = keyboard_label_keys
	if this._layout then
		keyboard_create_buttons(this)
		this:_label_keys()
	end
	return this
end

local key_label_map = {
	["#"] = "numbersign",
	["."] = "period",
	[","] = "comma",
	[":"] = "colon",
	[";"] = "semicolon",
	[" "] = "space",
	["!"] = "exclam",
	["\""]= "quotedbl",
	["§"] = "paragraph",
	["%"] = "percent",
	["&"] = "ampersand",
	["/"] = "slash",
	["\\"]= "backslash",
	["*"] = "asterisk",
	["+"] = "plus",
	["~"] = "tilde",
	["("] = "parenleft",
	[")"] = "parenright",
	["="] = "equal",
	["?"] = "question",
	["<"] = "less",
	[">"] = "greater",
	["|"] = "bar",
	["'"] = "apostrophe",
	["$"] = "dollar",
	["¥"] = "yen",
	["£"] = "sterling",
	["€"] = "EuroSign",
	["°"] = "degree",
	["^"] = "asciicircum",
	["®"] = "registered",
	["©"] = "copyright",
	["™"] = "trademark",
	["±"] = "plusminus",
	["⁰"] = "zerosuperior",
	["¹"] = "onesuperior",
	["²"] = "twosuperior",
	["³"] = "threesuperior",
	["⁴"] = "foursuperior",
	["⁵"] = "fivesuperior",
	["⁶"] = "sixsuperior",
	["⁷"] = "sevensuperior",
	["⁸"] = "eightsuperior",
	["⁹"] = "ninesuperior",
	["₀"] = "zerosubscript",
	["₁"] = "onesubscript",
	["₂"] = "twosubscript",
	["₃"] = "threesubscript",
	["₄"] = "foursubscript",
	["₅"] = "fivesubscript",
	["₆"] = "sixsubscript",
	["₇"] = "sevensubscript",
	["₈"] = "eightsubscript",
	["₉"] = "ninesubscript",
	["…"] = "ellipis",
}

local function type_key(key)
	local key = key_label_map[key] or key
	os.execute("xdotool key '"..key.."'")
end

local function keydown(key)
	local key = key_label_map[key] or key
	print("keydown '"..key.."'")
	os.execute("xdotool keydown '"..key.."'")
end

local function keyup(key)
	local key = key_label_map[key] or key
	print("keyup '"..key.."'")
	os.execute("xdotool keyup '"..key.."'")
end

local keyboards = {}
local keyboard = Stack{
	parent = main_window,
}

local function switch_layout(layout_name)
	local _keyboard = keyboards[layout_name]
	if _keyboard then
		keyboard.visible_child_name = layout_name
		for _,key in pairs(_keyboard._layout.keyup or {}) do
			keyup(key)
		end
	end
end

for name, layout in pairs(layouts) do
	keyboards[name] = Keyboard{
		parent = keyboard,
		position = name,
		_layout = layout,
		expand = true,
		row_homogeneous = true,
		column_homogeneous = true,
		halign = "fill",
		valign = "fill",
		class = "keyboard",
		_signal_type_key = type_key,
		_signal_keydown = keydown,
		_signal_keyup = keyup,
		_signal_set_layout = switch_layout,
	}
end

keyboard.visible_child_name = "letters"
