local function torch_controller_on_property_update(this, object, key, value)
	if key == "state" then
		this.widget.state = value
	end
end

local function TorchController(torch_widget, proxy_object)
	torch_widget._signal_clicked = function (widget)
		if widget.state == "off" then
			proxy_object:signal("turn_on")
		else
			proxy_object:signal("turn_off")
		end
	end
	local this = {
		widget = torch_widget,
		proxy_object = proxy_object,
		on_property_update = torch_controller_on_property_update,
	}
	proxy_object:subscribe(this)
	proxy_object:request "state"
	return this
end

local function wifi_button_controller_on_property_update(this, object, key, value)
	if key == "signal_strength" then
		if this.widget.state:match("^active_?%d*$") then
			local strength = tonumber(value)
			if strength <= 12 then
				this.widget.state = "active_00"
			elseif strength <= 37 then
				this.widget.state = "active_25"
			elseif strength <= 62 then
				this.widget.state = "active_50"
			elseif strength <= 87 then
				this.widget.state = "active_75"
			else
				this.widget.state = "active_100"
			end
		end
	elseif key == "network_state" then
		if value == "disconnected" then
			this.widget.state = "disconnected"
		elseif value == "connecting" then
			this.widget.state = "connecting"
		elseif value == "access_point" then
			this.widget.state = "active_ap"
		elseif value == "connected" then
			this.widget.state = "active"
		else
			this.widget.state = "active_no_route"
		end
	end
end

local function WifiButtonController(wifi_button, proxy_object)
	local this = {
		widget = wifi_button,
		proxy_object = proxy_object,
		on_property_update = wifi_button_controller_on_property_update,
	}
	proxy_object:subscribe(this)
	proxy_object:request "network_state"
	proxy_object:request "wifi_signal_strength"
	return this
end

local function bluetooth_button_controller_on_property_update(this, object, key, value)
	if key == "enabled" then
		this._enabled = (value == "yes")
	elseif key == "bluetooth_state" then
		this._paired = (value == "paired")
	end
	if this._enabled then
		if this._paired then
			this.widget.state = "paired"
		else
			this.widget.state = "active"
		end
	else
		this.widget.state = "disabled"
	end
end

local function BluetoothButtonController(bluetooth_button, proxy_object)
	local this = {
		widget = bluetooth_button,
		proxy_object = proxy_object,
		on_property_update = bluetooth_button_controller_on_property_update,
	}
	proxy_object:subscribe(this)
	proxy_object:request "enabled"
	proxy_object:request "bluetooth_state"
	return this
end

local function service_button_controller_on_property_update(this, object, key, value)
	if key == "active" then
		this._active = (value == "yes")
		if value == "yes" then
			this.widget.state = "active"
		else
			this.widget.state = "inactive"
		end
	end
end

local function ServiceButtonController(service_button, proxy_object)
	local this = {
		widget = service_button,
		proxy_object = proxy_object,
		on_property_update = service_button_controller_on_property_update,
	}
	proxy_object:subscribe(this)
	proxy_object:request "active"
	service_button._signal_clicked = function ()
		if this._active then
			this.proxy_object:signal "stop"
		else
			this.proxy_object:signal "start"
		end
	end
	return this
end

local function notification_controller_on_property_update(this, object, key, value)
	if key == "title" then
		this.widget.title = value
		this.title = value
	elseif key == "description" then
		this.widget.description = value
		this.description = value
	elseif key == "application_id" then
		this.application_id = value
	elseif key == "time_last_updated" then
		this.time_last_updated = tonumber(value)
		this.widget.relevancy_label = os.date("%m-%d %H:%M", this.time_last_updated)
		this.widget._timestamp = this.time_last_updated
	elseif key == "urgency" then
		this.urgency = value
		if value == "high" then
			this.widget:call("add_style_class","high-urgency")
			this.widget._urgency = 1
		else
			this.widget:call("remove_style_class","high-urgency")
			this.widget._urgency = 0
		end
	elseif key == "is_hidden" then
		--used by an external controller to show and hide widgets
		this.is_hidden = (value == "true")
		this.widget._is_hidden = this.is_hidden
	elseif key == "is_dismissable" then
		this.is_dismissable = (value ~= "false")
	elseif key:match("^command_action:[1-5]$") or key:match("^command_label:[1-5]$") or key:match("^command_name:[1-5]$") then
		local cmd, n = key:match("^command_(.-):(%d+)$")
		local n = tonumber(n)
		-- create a button array for the notification widget if it doesn't exist
		if not this.widget._buttons then
			this.widget._buttons = NotificationActionButtons{
				parent = this.widget,
				position = "end.no-expand.no-fill",
				class = "action-buttons",
			}
			this.widget._buttons._signal_action = function(widget, action_n)
				local command_desc = this.commands[action_n]
				if command_desc then
					if command_desc.action then
						local cmd, arg = command_desc.action:match("^([^ ]+) ?(.*)$")
						if cmd == "signal" then
							this.proxy_object:signal("command_action:"..action_n)
						elseif cmd == "uri" and arg ~= "" then
							--TODO: replace with a better alternative/make configurable
							os.execute("xdg-open '"..arg:gsub("'","").."'")
						end
					end
				end
			end
		end
		this.commands[n][cmd] = value
		if cmd == "label" then
			this.widget._buttons:set_label(n, value)
		elseif cmd == "name" then
			--TODO: do some fun rule matching to get icons for the buttons
		end
	elseif key == "type" then
		if value == "" then
			this.widget:remove()
			this.proxy_object:unsubscribe(this)
		end
	end
	if this._signal_hook_property_update then
		this:_signal_hook_property_update(object, key, value)
	end
end

local function NotificationController(notification_widget, proxy_object)
	local this = {
		widget = notification_widget,
		proxy_object = proxy_object,
		on_property_update = notification_controller_on_property_update,
		is_dismissable = false, --wait for response util we allow dismissing
		is_hidden = false,
		commands = {},
	}
	for i=1,5 do
		this.commands[i] = {
			label = "",
			name = tostring(i),
			action = "",
		}
	end
	notification_widget._signal_close = function ()
		if this.is_dismissable then
			this.proxy_object:set("type", nil)
		else
			this.proxy_object:set("is_hidden", true)
		end
	end
	--TODO: hook unsubscribe into gc
	proxy_object:subscribe(this)
	proxy_object:request("title")
	proxy_object:request("description")
	proxy_object:request("urgency")
	proxy_object:request("is_hidden")
	proxy_object:request("is_dismissable")
	proxy_object:request("time_last_updated")
	proxy_object:request("application_id")
	for i=1,5 do
		proxy_object:request("command_label:"..i)
		proxy_object:request("command_action:"..i)
		proxy_object:request("command_name:"..i)
	end
	return this
end

local function notification_widget_sort_function(a,b)
	if (a._is_hidden or false) ~= (b._is_hidden or false) then
		return b._is_hidden
	end
	if (a._is_title or false) ~= (b._is_title or false) then
		return a._is_title
	end
	if (a._urgency or 0) ~= (b._urgency or 0) then
		return (a._urgency or 0) > (b._urgency or 0)
	end
	if (a._timestamp or math.huge) ~= (b._timestamp or math.huge) then
		return (a._timestamp or math.huge) > (b._timestamp or math.huge)
	end
	return a.title > b.title
end

local function notification_inbox_controller_on_property_update(this, object, key, value)
	if key == "active_list" then
		local unchecked = {}
		for k,_ in pairs(this.notification_widgets) do
			unchecked[k] = true
		end
		-- a very hacky split an iterate with callback
		value:gsub("[^ ]+", function (name)
			unchecked[name] = false
			-- add widgets that don't exist yet
			if not this.notification_widgets[name] then
				local notification = NotificationWidget(copy_table(this.attr))
				local controller = NotificationController(notification, object.connection:get_proxy_object(name))
				controller._signal_hook_property_update = this._notification_property_hook
				this.notification_widgets[name] = notification
			end
		end)
		-- remove widgets that don't exist anymore
		-- and prepare the ones still present for sorting
		for name,uc in pairs(unchecked) do
			if uc then
				this.notification_widgets[name]:remove()
				this.notification_widgets[name] = nil
			end
		end
		this:sort_widgets()
	end	
end

local function notification_inbox_controller_sort_widgets()
	local notification_widget_list = {}
	for name,widget in pairs(this.notification_widgets) do
		notification_widget_list[#notification_widget_list+1] = widget
	end
	table.sort(notification_widget_list, notification_widget_sort_function)
	for i = 1,#notification_widget_list do
		local widget = notification_widget_list[i]
		widget:unparent()
		this.attr.parent:add_widget(widget, this.attr.position)
	end
end

local function NotificationInboxController(attr, proxy_object)
	this = {
		attr = attr,
		proxy_object = proxy_object,
		on_property_update = notification_inbox_controller_on_property_update,
		_notification_property_hook = attr._notification_property_hook,
		notification_widgets = {},
		sort_widgets = notification_inbox_controller_sort_widgets,
	}
	attr._notification_property_hook = false
	proxy_object:subscribe(this)
	proxy_object:request("active_list")
	return this
end

-----------------------------------------------------------
--- Start defining actual widgets ------------------------
---------------------------------------------------------

main_window = Window{ label = "test", _signal_destroy = quit }
local wrapper_box = Box{ parent=main_window, orientation="v" }
local quick_action_box = Box{
	parent = wrapper_box,
	position = "start.no-fill.no-expand",
	orientation = "h",
	halign = "center",
}

local service_button_box = Box{
	parent = wrapper_box,
	position = "start.no-fill.no-expand",
	orientation = "h",
	halign = "center",
}

local notification_scroll = ScrolledWindow{
	parent = wrapper_box,
	position = "start",
	hscroll = "never",
	overlay = true,
}

local notification_box = Box{
	parent = notification_scroll,
	orientation = "v",
}

quit_button = Button{
	parent = wrapper_box,
	position = "end.no-fill.no-expand",
	_signal_clicked = quit,
	icon = "go-up-symbolic",
}

local big_button_icon_size = 48

local wifi_button = WifiButton{
	parent = quick_action_box,
	icon_size = big_button_icon_size,
	class = "big-quick-button",
}


local wifi_popover = Popover{
	relative_to = wifi_button.id,
	modal = true,
	popdown = true,
}

wifi_button._signal_clicked = function()
	wifi_popover:call("popup")
end

local popover_label = Label{
	parent = wifi_popover,
	label = "Lorem Ipsum Dolor sid …",
}

local torch_button = TorchButton{
	parent = quick_action_box,
	icon_size = big_button_icon_size,
	class = "big-quick-button",
}

local bluetooth_button = BluetoothButton{
	parent = quick_action_box,
	icon_size = big_button_icon_size,
	class = "big-quick-button",
}

local service_telegram = ServiceButton{
	parent = service_button_box,
	icon = "telegram-panel",
	icon_notify = "telegram-attention-panel",
}

local service_syncthing = ServiceButton{
	parent = service_button_box,
	icon = "si-syncthing-0",
}

local service_irc = ServiceButton{
	parent = service_button_box,
	icon = "irc-channel-joined",
	icon_inactive = "irc-channel-parted"
}

local service_mpd = ServiceButton{
	parent = service_button_box,
	icon = "QMPlay2-panel",
}

local service_onboard = ServiceButton{
	parent = service_button_box,
	icon = "onboard-panel"
}

--[[
local test_player_notification = MediaPlayerWidget{
	parent = notification_box,
	position = "start.no-fill.no-expand",
	title = "MPD (local)",
	description = "Lorem Ipsum - a Synthwave mix",
	_signal_close = function () print("Notification close") end,
	_signal_clicked = function() print("Notification clicked") end,
	has_close_button = false,
}
--]]

function write_to_stdout(this, str)
	output(str)
end

local icons = {
	["Telegram Desktop"] = "telegram-panel",
	["Firefox"] = "firefox-symbolic",
	["NetworkManager"] = "network-transmit-receive",
	["Thunderbird"] = "mail-unread-symbolic",
}

connection = DDBConnection({write = write_to_stdout})

local function notification_property_hook(notification, object, key, value)
	if key == "application_id" then
		if notification.application_id == "NetworkManager" then
			if notification.title:match("Disconnected") then
				notification.widget.icon = "network-offline"
			else
				notification.widget.icon = "network-transmit-receive"
			end
		else
			notification.widget.icon = icons[notification.application_id]
		end
	end
	if key == "time_last_updated" or key == "urgency" or key == "is_hidden" then
		notification_inbox_controller:sort_widgets()
	end
end

notification_inbox_controller = NotificationInboxController({
	parent = notification_box,
	position = "start.no-fill.no-expand",
	_notification_property_hook = notification_property_hook,
},connection:get_proxy_object("exni"))

local torch_proxy = connection:get_proxy_object("torch")
local wifi_proxy = connection:get_proxy_object("wifi")
local bluetooth_proxy = connection:get_proxy_object("bluetooth")

local telegram_proxy = connection:get_proxy_object("service.telegram")
local irc_proxy = connection:get_proxy_object("service.irc")
local mpd_proxy = connection:get_proxy_object("service.mpd")
local onboard_proxy = connection:get_proxy_object("service.onboard")
local syncthing_proxy = connection:get_proxy_object("service.syncthing")

local torch_controller = TorchController(torch_button, torch_proxy)
local wifi_controller = WifiButtonController(wifi_button, wifi_proxy)
local bluetooth_controller = BluetoothButtonController(bluetooth_button, bluetooth_proxy)
local service_tg_controller = ServiceButtonController(service_telegram, telegram_proxy)
local service_irc_controller = ServiceButtonController(service_irc, irc_proxy)
local service_mpd_controller = ServiceButtonController(service_mpd, mpd_proxy)
local service_ob_controller = ServiceButtonController(service_onboard, onboard_proxy)
local service_sync_constroller = ServiceButtonController(service_syncthing, syncthing_proxy)

function on_stdin_line(line)
	connection:eval_line(line)
end

collectgarbage()
