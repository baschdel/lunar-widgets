function make_weak_table()
	local t = {}
	setmetatable(t, { __mode="v" })
	return t
end

function copy_table(t)
	local o = {}
	for k,v in pairs(t) do
		if type(v) == "table" then
			o[k] = copy_table(v)
		else
			o[k] = v
		end
	end
	return o
end

-- a table that contains weak references to all widgets
local widgets = make_weak_table()

local function widget_unparent(this)
	local parent = widgets[this.parent_id]
	if parent then
		if parent.children[this.position] then
			parent.children[this.position][this.id] = nil
		end
	end
	unparent_widget(this.id)
	this.parent_id = nil
	this.position = nil
end

local function widget_remove(this)
	for position,children in pairs(this.children or {}) do
		for _,child in pairs(children) do
			widget_remove(child)
		end
	end
	widget_unparent(this)
	remove_widget(this.id)
end

local function widget_call(this, command, arg)
	return widget_command(this.id, command, arg)
end

local function widget_add_widget(this, widget, position)
	assert(widget.widget == "yes", "'widget' argument to widget:add_widget() must be a widget!")
	unparent_widget(widget)
	local position = tostring(position)
	if not this.children[position] then
		this.children[position] = {}
	end
	local children_table = this.children[position]
	children_table[widget.id] = widget
	assign_widget(widget.id, this.id, position)
	rawset(widget, "parent_id", this.id)
	rawset(widget, "position", position)
end

local function widget_setvalue(this, key, value)
	-- allow signal handlers to be registred
	--print("widget_setvalue("..tostring(this)..", '"..tostring(key).."', '"..tostring(value).."')")
	local setter = rawget(this, "_set_"..key)
	if setter then
		setter(this, value)
	else
		local signal_name = key:match("^_signal_(.+)$")
		if signal_name then
			this.signal[signal_name] = value
		end
		if key:match("^_") then
			rawset(this, key, value)
		else
			widget_command(this.id, key, tostring(value))
		end
	end
end

local function widget_getvalue(this, key)
	if key == "parent" then
		return widgets[this.parent_id]
	end
	local getter = rawget(this, "_get_"..key)
	if getter then
		return getter(this)
	end
	local signal_name = key:match("^_signal_(.+)$")
	if signal_name then
		return this.signal[signal_name]
	end
	if key:match("^_") then return nil end
	return widget_command(this.id, key)
end

local function widget_tostring(this)
	if this.parent_id then
		return this.id.." = Widget('"..this.type.."', '"..this.parent_id.."', '"..tostring(this.position).."')"
	else
		return this.id.." = Widget('"..this.type.."', no_parent)"
	end
end

local function widget_eval_attribute_table(this, attributes)
	for k,v in pairs(attributes) do
		if k == "parent" then
			v:add_widget(this, attributes.position)
		elseif k == "position" then
			-- noop
		elseif k == "class" then
			widget_call(this, "add_style_class", v)
		elseif k == "classes" then
			for _,class in pairs(v) do
				widget_call(this, "add_style_class", class)
			end
		else
			this[k] = v
		end
	end
end

function Widget(widget_type, attributes)
	local this = {}
	this.type = widget_type
	this.id = uuid()
	this.parent_id = (attributes.parent or {})["id"]
	this.position = attributes.position
	this.children = {}
	this.signal = make_weak_table()
	this.widget = "yes"
	-- add utility functions
	this.remove = widget_remove
	this.unparent = widget_unparent
	this.call = widget_call
	this.add_widget = widget_add_widget
	-- set metatable
	local metatable = {}
	metatable.__gc = widget_remove
	metatable.__index = widget_getvalue
	metatable.__newindex = widget_setvalue
	metatable.__tostring = widget_tostring
	setmetatable(this, metatable)
	-- add widget to global widget table
	widgets[this.id] = this
	-- create the actual widget
	make_widget(this.type, this.id)
	widget_eval_attribute_table(this, attributes)
	return this
end

function on_widget_signal(source_id, signal, data)
	local widget = widgets[source_id]
	if widget then
		local callback = widget.signal[signal]
		if callback then
			callback(widget, data)
			return
		end
	end
end

local function make_widget_constructor(widget_type)
	return function (attr)
		return Widget(widget_type, attr)
	end
end

Window = make_widget_constructor("window")
Box = make_widget_constructor("box")
Label = make_widget_constructor("label")
Image = make_widget_constructor("image")
ToggleButton = make_widget_constructor("toggle")
ScrolledWindow = make_widget_constructor("scrolled")
Popover = make_widget_constructor("popover")
Grid = make_widget_constructor("grid")
Stack = make_widget_constructor("stack")

local function add_image_based_on_attr(to_widget, attr)
	if attr.icon then
		local image = Image{
			icon = attr.icon,
			size = attr.icon_size,
		}
		to_widget._set_icon = function(this, icon)
			image.icon = icon
		end
		to_widget._get_icon = function(this)
			return image.icon
		end
		to_widget:add_widget(image, "image")
	end
end

function Button(attr)
	local button_widget = Widget("button", attr)
	add_image_based_on_attr(button_widget, attr)
	return button_widget
end

function ToggleButton(attr)
	local button_widget = Widget("toggle", attr)
	add_image_based_on_attr(button_widget, attr)
	return button_widget
end

-----------------------------------------------------------
--- Timer ------------------------------------------------
---------------------------------------------------------

local function timer_stop(this)
	this.is_running = false
end

local function timer_handle_tick(this)
	if this.is_running then
		this:tick()
	end
	-- check if the timer was stopped by its handler
	if this.is_running then
		this.is_running = false
		if this.loop then
			this:start()
		end
	end
end

local function timer_start(this)
	if not this.is_running then
		this.is_running = true
		timeout(this.callback_name, this.interval)
	end
end

local function timer_gc(this)
	this:stop()
	_G[this.callback_name] = nil
	_G[this.callback_name.."_this"] = nil
end

function Timer(interval, loop, tick)
	local this = {}
	this.id = uuid()
	this.callback_name = "_timer_object_callback_"..this.id.."_"
	this.is_running = false
	this.tick = tick
	this.loop = loop
	this.interval = interval
	this.start = timer_start
	this.stop = timer_stop
	this.handle_tick = timer_handle_tick
	this.destroy = timer_gc
	local metatable = {
		__gc = timer_gc,
	}
	setmetatable(this, metatable)
	-- todo: wrap this in a weak table
	local weak_pointer = { this=this }
	setmetatable(weak_pointer, { __mode="v" })
	local global_this_pointer_name = this.callback_name.."_this"
	_G[global_this_pointer_name] = weak_pointer
	_G[this.callback_name] = function () _G[global_this_pointer_name].this:handle_tick() end
	return this
end

-- animates an icon until the given property of the widget changes its state
function animate_icon_as_long_as(widget, icons, fps, property, state)
	local animation_counter = 1
	local timer = Timer(1/fps, true, function(timer)
		if widget[property] == state then
			local icon = icons[animation_counter]
			animation_counter = animation_counter+1
			if animation_counter > #icons then
				animation_counter = 1
			end
			widget.icon = icon
		else
			timer:destroy()
		end
	end)
	timer:start()
end

-----------------------------------------------------------
--- StateButton ------------------------------------------
---------------------------------------------------------

local function state_button_set_state(this, state)
	local looks = (this._looks or {})[state]
		if looks then
		if looks.active then
			this:call("add_style_class", "suggested-action")
		else
			this:call("remove_style_class", "suggested-action")
		end
		this._state = state
		if looks.animation and not _global_setting_disable_animation then
			animate_icon_as_long_as(this, looks.animation, looks.fps or 12, "_state", state)
		else
			if looks.icon then
				this.icon = looks.icon
			end
		end
	end
end

local function state_button_get_state(this)
	return this._state
end

function StateButton(attr)
	local this = Button(attr)
	this._set_state = state_button_set_state
	this._get_state = state_button_get_state
	this.state = attr.state
	return this
end
