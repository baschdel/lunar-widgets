-- a table that contains weak references to all widgets
local widgets = {}
setmetatable(widgets, { __mode="v" })

local function widget_unparent(this)
	local parent = widgets[this.parent_id]
	if parent then
		parent.children[this.position][this.id] = nil
	end
	unparent_widget(this.id)
	this.parent_id = nil
	this.position = nil
end

local function widget_remove(this)
	for position,children in pairs(this.children or {}) do
		for _,child in pairs(children) do
			widget_remove(child)
		end
	end
	widget_unparent(this)
	remove_widget(this.id)
end

local function widget_call(this, command, arg)
	return widget_command(this.id, command, args)
end

local function widget_add_widget(this, widget, position)
	assert(widget.widget == "yes", "'widget' argument to widget:add_widget() must be a widget!")
	unparent_widget(widget)
	local position = tostring(position)
	if not this.children[position] then
		this.children[position] = {}
	end
	local children_table = this.children[position]
	children_table[widget.id] = widget
	assign_widget(widget.id, this.id, position)
	rawset(widget, "parent_id", this.id)
	rawset(widget, "position", position)
end

local function widget_setvalue(this, key, value)
	-- allow signal handlers to be registred
	if key:match("^_") then
		rawset(this, key, value)
	else
		local setter = this["_set_"..key]
		if setter then
			setter(this, value)
		else
			widget_command(this.id, key, value)
		end
	end
end

local function widget_getvalue(this, key)
	if key:match("^_") then return nil end
	if key == "parent" then
		return widgets[this.parent_id]
	end
	local getter = this["_get_"..key]
	if getter then
		return getter(this)
	end
	return widget_command(this.id, key)
end

local function widget_tostring(this)
	if this.parent_id then
		return this.id.." = Widget('"..this.type.."', '"..this.parent_id.."', '"..tostring(this.position).."')"
	else
		return this.id.." = Widget('"..this.type.."', no_parent)"
	end
end

local function widget_eval_attribute_table(this, attributes)
	for k,v in pairs(attributes) do
		if k == "parent" then
			widget_add_widget(v, this, attributes.position)
		elseif k == "position" then
			-- noop
		elseif k == "class" then
			widget_call(this, "add_style_class", v)
		elseif k == "classes" then
			for _,class in pairs(v) do
				widget_call(this, "add_style_class", class)
			end
		else
			this[k] = v
		end
	end
end

function Widget(widget_type, attributes)
	local this = {}
	this.type = widget_type
	this.id = uuid()
	this.parent_id = (attributes.parent or {})["id"]
	this.position = attributes.position
	this.children = {}
	this.widget = "yes"
	-- add utility functions
	this.remove = widget_remove
	this.unparent = remove_widget_from_parent
	this.call = widget_call
	this.add_widget = widget_add_widget
	-- set metatable
	local metatable = {}
	metatable.__gc = widget_remove
	metatable.__index = widget_getvalue
	metatable.__newindex = widget_setvalue
	metatable.__tostring = widget_tostring
	setmetatable(this, metatable)
	-- add widget to global widget table
	widgets[this.id] = this
	-- create the actual widget
	make_widget(this.type, this.id)
	widget_eval_attribute_table(this, attributes)
	return this
end

function on_widget_signal(source_id, signal, data)
	local widget = widgets[source_id]
	if widget then
		local callback = widget["_signal_"..signal]
		if callback then
			callback(widget, data)
			return
		end
	end
end

local function make_widget_constructor(widget_type)
	return function (attr)
		return Widget(widget_type, attr)
	end
end

local Window = make_widget_constructor("window")
local Box = make_widget_constructor("box")
local Label = make_widget_constructor("label")
local Image = make_widget_constructor("image")
local ToggleButton = make_widget_constructor("toggle")

local function add_image_based_on_attr(to_widget, attr)
	if attr.icon then
		image = Image{
			icon = attr.icon,
			size = attr.icon_size,
		}
		to_widget._set_icon = function(this, icon)
			image.icon = icon
		end
		to_widget._get_icon = function(this)
			return image.icon
		end
		to_widget:add_widget(image)
	end
end

local function Button(attr)
	local button_widget = Widget("button", attr)
	add_image_based_on_attr(button_widget, attr)
	return button_widget
end

local function ToggleButton(attr)
	local button_widget = Widget("toggle", attr)
	add_image_based_on_attr(button_widget, attr)
	return button_widget
end

----- END OF WIDGET FRAMEWORK -----

local function callback(f,...)
	local args = {...}
	return function ()
		f(table.unpack(args))
	end
end

function on_stdin_line(line)
	print("Received input from stdin: "..line)
end

function cycle_icons(widget)
	local icon = widget.icon
	if icon == "gnome-netstatus-disconn" then
		widget.icon = "gnome-netstatus-0-24"
	elseif icon == "gnome-netstatus-0-24" then
		widget.icon = "gnome-netstatus-25-49"
	elseif icon == "gnome-netstatus-25-49" then
		widget.icon = "gnome-netstatus-50-74"
	elseif icon == "gnome-netstatus-50-74" then
		widget.icon = "gnome-netstatus-75-100"
	else
		widget.icon = "gnome-netstatus-disconn"
	end
end

local main_window = Window{ label = "test", _signal_destroy = quit }
local btn = Button{ parent = main_window, label = "foo bar baz", icon = "network-disconnected", _signal_clicked = cycle_icons}

