public class LunarWidgets.CommandHelper {
	
	public static string? execute_generic_widget_command(string command, string? data, Gtk.Widget widget){
		switch (command) {
			case "add_style_class":
				if (data != null) {
					widget.get_style_context().add_class(data);
					return "ok";
				}
				return null;
			case "remove_style_class":
				if (data != null) {
					widget.get_style_context().remove_class(data);
					return "ok";
				}
				return null;
			case "sensitive":
				if (data != null) {
					widget.sensitive = string_to_bool(data);
				}
				return bool_to_string(widget.sensitive);
			case "visible":
				if (data != null) {
					widget.visible = string_to_bool(data);
				}
				return bool_to_string(widget.visible);
			case "show":
				widget.show();
				return "ok";
			case "hide":
				widget.hide();
				return "ok";
			case "margin":
				if (data != null)	{
					widget.margin = parse_integer(data, widget.margin);
				}
				return widget.margin.to_string();
			case "add_css":
				if (data != null) {
					try {
						print("Loading custom css: \n"+data+"\n");
						var provider = new Gtk.CssProvider();
						provider.load_from_data(data);
						widget.get_style_context().add_provider(provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);
						return "ok";
					} catch (Error e) {
						return e.message;
					}
				}
				return null;
			case "halign":
				if (data != null) {
					widget.halign = string_to_align(data);
				}
				return null;
			case "valign":
				if (data != null) {
					widget.valign = string_to_align(data);
				}
				return null;
			default:
				return null;
		}
	}
	
	
	public static string? execute_orientable_widget_command(string command, string? data, Gtk.Orientable widget){
		switch(command) {
			case "orientation":
				if (data == "h" || data == "horizontal") {
					widget.orientation = HORIZONTAL;
				} else if (data == "v" || data == "vertical") {
					widget.orientation = VERTICAL;
				}
				if (widget.orientation == HORIZONTAL) {
					return "horizontal";
				} else {
					return "vertical";
				}
			default:
				return null;
		}
	}
	
	public static int? parse_integer(string number, int? default_value) {
		int result = 0;
		if (int.try_parse(number, out result)) {
			return result;
		} else {
			return default_value;
		}
	}
	
	public static bool string_to_bool(string? boolean) {
		return (boolean == "true" || boolean == "yes" || boolean == "y");
	}
	
	public static string bool_to_string(bool val) {
		return val ? "true":"false";
	}
	
	public static Gtk.Align string_to_align(string? str) {
		switch (str){
			case "fill":
				return FILL;
			case "end":
				return END;
			case "center":
				return CENTER;
			case "start":
			default:
				return START;
		}
	}
	
	public static Pango.EllipsizeMode string_to_ellipsize_mode(string? str) {
		switch (str) {
			case "start":
				return START;
			case "middle":
				return MIDDLE;
			case "end":
				return END;
			case "none":
			default:
				return NONE;
		}
	}
	
	public static Gtk.Justification string_to_justification(string? str) {
		switch (str) {
			case "right":
				return RIGHT;
			case "fill":
				return FILL;
			case "center":
				return CENTER;
			case "left":
			default:
				return LEFT;
		}
	}
}
