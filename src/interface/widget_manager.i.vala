public interface LunarWidgets.Interface.WidgetManager : Object {
	
	public signal void deliver_signal(string from_widget_id, string name, string? data);
	
	public abstract LunarWidgets.Interface.Widget? get_widget(string widget_id);
	public abstract bool add_widget(LunarWidgets.Interface.Widget widget);
	
	public abstract bool remove_widget(string widget_id);
	public abstract bool unparent_widget(string widget_id);
	public abstract bool assign_widget(string widget_id, string parent_id, string position);
	
	public abstract string? try_execute_command(string widget_id, string command, string? data);
	
}
