public class LunarWidgets.WidgetFactory : Object {
	
	public LunarWidgets.Interface.WidgetManager wm;
	
	public WidgetFactory(LunarWidgets.Interface.WidgetManager wm) {
		this.wm = wm;
	}
	
	public bool produce(string widget_type, string id, string parent_id, string position) {
		LunarWidgets.Interface.Widget widget;
		switch(widget_type) {
			case "button":
				widget = new LunarWidgets.Widget.Button(wm, id);
				break;
			case "toggle":
				widget = new LunarWidgets.Widget.ToggleButton(wm, id);
				break;
			case "label":
				widget = new LunarWidgets.Widget.Label(wm, id);
				break;
			case "image":
				widget = new LunarWidgets.Widget.Image(wm, id);
				break;
			case "box":
				widget = new LunarWidgets.Widget.Box(wm, id);
				break;
			case "grid":
				widget = new LunarWidgets.Widget.Grid(wm, id);
				break;
			case "scrolled":
				widget = new LunarWidgets.Widget.ScrolledWindow(wm, id);
				break;
			case "stack":
				widget = new LunarWidgets.Widget.Stack(wm, id);
				break;
			case "window":
				widget = new LunarWidgets.Widget.Window(wm, id);
				break;
			case "popover":
				widget = new LunarWidgets.Widget.Popover(wm, id);
				break;
			default:
				return false;
		}
		if (wm.add_widget(widget)){
			widget.show_all();
			if (parent_id != "nil") {
				return wm.assign_widget(id, parent_id, position);
			} else {
				return true;
			}
		}
		return false;
	}
	
}
