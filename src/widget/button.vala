public class LunarWidgets.Widget.Button : Gtk.Button, LunarWidgets.Interface.Widget {
	
	public string id;
	public LunarWidgets.Interface.WidgetManager wm;
	
	public Button(LunarWidgets.Interface.WidgetManager wm, string id) {
		this.wm = wm;
		this.id = id;
		this.clicked.connect(on_clicked);
	}
	
	private void on_clicked() {
		wm.deliver_signal(id, "clicked", null);
	}
	
	  ///////////////////////////////////
	 // LunarWidgets.Interface.Widget //
	///////////////////////////////////
	
	public string get_widget_id(){
		return this.id;
	}
	
	public bool add_widget(string position, Gtk.Widget widget){
		if (position == "image") {
			this.image = widget;
			this.image.visible = true;
		} else {
			this.add(widget);
			widget.visible = true;
		}
		return true;
	}
	
	public string? command(string command, string? data) {
		string? result;
		result = LunarWidgets.CommandHelper.execute_generic_widget_command(command, data, this);
		if (result != null) { return result; }
		switch(command) {
			case "label":
				if (data != null) {
					this.label = data;
				}
				return this.label;
			default:
				return null;
		}
	}
	
}
