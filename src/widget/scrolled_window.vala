public class LunarWidgets.Widget.ScrolledWindow : Gtk.ScrolledWindow, LunarWidgets.Interface.Widget {
	
	public string id;
	public LunarWidgets.Interface.WidgetManager wm;
	
	private Gtk.GestureDrag drag_gesture;
	private double drag_start_x = 0;
	private double drag_start_y = 0;
	
	public ScrolledWindow(LunarWidgets.Interface.WidgetManager wm, string id) {
		this.wm = wm;
		this.id = id;
		this.drag_gesture = new Gtk.GestureDrag(this);
		drag_gesture.drag_begin.connect(on_drag_start);
		drag_gesture.drag_update.connect(on_drag_update);
		this.show();
	}
	
	private void on_drag_start(double pos_x, double pos_y){
		drag_start_x = this.hadjustment.value;
		drag_start_y = this.vadjustment.value;
	}
	
	private void on_drag_update(double offset_x, double offset_y){
		this.hadjustment.value = drag_start_x - offset_x;
		this.vadjustment.value = drag_start_y - offset_y;
	}
	
	  ///////////////////////////////////
	 // LunarWidgets.Interface.Widget //
	///////////////////////////////////
	
	public string get_widget_id(){
		return this.id;
	}
	
	public bool add_widget(string position, Gtk.Widget widget){
		this.add(widget);
		return true;
	}
	
	public string? command(string command, string? data) {
		string? result;
		result = LunarWidgets.CommandHelper.execute_generic_widget_command(command, data, this);
		if (result != null) { return result; }
		switch(command) {
			case "kinetic":
				if (data != null) {
					this.kinetic_scrolling = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.kinetic_scrolling);
			case "overlay":
				if (data != null) {
					this.overlay_scrolling = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.overlay_scrolling);
			case "hscroll":
				if (data != null) {
					this.hscrollbar_policy = string_to_scroll_policy_type(data);
				}
				return scroll_polycy_type_to_string(this.hscrollbar_policy);
			case "vscroll":
				if (data != null) {
					this.vscrollbar_policy = string_to_scroll_policy_type(data);
				}
				return scroll_polycy_type_to_string(this.vscrollbar_policy);
			default:
				return null;
		}
	}
	
	private Gtk.PolicyType string_to_scroll_policy_type(string str) {
		switch (str) {
			case "always":
				return ALWAYS;
			case "never":
				return NEVER;
			case "automatic":
			default:
				return AUTOMATIC;
		}
	}
	
	private string scroll_polycy_type_to_string(Gtk.PolicyType pt) {
		switch (pt) {
			case ALWAYS:
				return "always";
			case AUTOMATIC:
				return "automatic";
			case NEVER:
				return "never";
			case EXTERNAL:
				return "external";
			default:
				return "unknown";
		}
	}
	
}
