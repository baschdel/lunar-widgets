public class LunarWidgets.Widget.Image : Gtk.Image, LunarWidgets.Interface.Widget {
	
	public string id;
	public LunarWidgets.Interface.WidgetManager wm;
	
	public Image(LunarWidgets.Interface.WidgetManager wm, string id) {
		this.wm = wm;
		this.id = id;
		this.set_from_icon_name("image-loading", BUTTON);
		this.show();
	}
	
	  ///////////////////////////////////
	 // LunarWidgets.Interface.Widget //
	///////////////////////////////////
	
	public string get_widget_id(){
		return this.id;
	}
	
	public string? command(string command, string? data) {
		string? result;
		result = LunarWidgets.CommandHelper.execute_generic_widget_command(command, data, this);
		if (result != null) { return result; }
		switch(command) {
			case "icon":
				if (data != null) {
					this.icon_name = data;
					this.visible = true;
				}
				return this.icon_name;
			case "file":
				if (data != null) {
					this.file = data;
					this.visible = true;
				}
				return this.file;
			case "size":
				if (data != null)	{
					this.pixel_size = LunarWidgets.CommandHelper.parse_integer(data, this.pixel_size);
				}
				return this.pixel_size.to_string();
			default:
				return null;
		}
	}
	
}
