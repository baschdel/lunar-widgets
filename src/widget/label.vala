public class LunarWidgets.Widget.Label : Gtk.Label, LunarWidgets.Interface.Widget {
	
	public string id;
	public LunarWidgets.Interface.WidgetManager wm;
	
	public Label(LunarWidgets.Interface.WidgetManager wm, string id) {
		this.wm = wm;
		this.id = id;
		this.wrap_mode = WORD_CHAR;
	}
	
	  ///////////////////////////////////
	 // LunarWidgets.Interface.Widget //
	///////////////////////////////////
	
	public string get_widget_id(){
		return this.id;
	}
	
	public string? command(string command, string? data) {
		string? result;
		result = LunarWidgets.CommandHelper.execute_generic_widget_command(command, data, this);
		if (result != null) { return result; }
		switch(command) {
			case "label":
				if (data != null) {
					this.label = data;
				}
				return this.label;
			case "wrap":
				if (data != null) {
					this.wrap = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.wrap);
			case "ellipsize":
				if (data != null) {
					this.ellipsize = LunarWidgets.CommandHelper.string_to_ellipsize_mode(data);
				}
				return null;
			case "justify":
				if (data != null) {
					this.justify = LunarWidgets.CommandHelper.string_to_justification(data);
				}
				return null;
			case "single_line_mode":
				if (data != null) {
					this.single_line_mode = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.single_line_mode);
			case "selectable":
				if (data != null) {
					this.selectable = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.selectable);
			case "is_markup":
				if (data != null) {
					this.use_markup = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.use_markup);
			default:
				return null;
		}
	}
	
}
