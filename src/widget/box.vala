public class LunarWidgets.Widget.Box : Gtk.Box, LunarWidgets.Interface.Widget {
	
	public string id;
	public LunarWidgets.Interface.WidgetManager wm;
	
	public Box(LunarWidgets.Interface.WidgetManager wm, string id) {
		this.wm = wm;
		this.id = id;
	}
	
	  ///////////////////////////////////
	 // LunarWidgets.Interface.Widget //
	///////////////////////////////////
	
	public string get_widget_id(){
		return this.id;
	}
	
	public bool add_widget(string position, Gtk.Widget widget){
		if (position.has_prefix("end")) {
			this.pack_end(widget, !position.contains(".no-expand"), !position.contains(".no-fill"));
		} else if (position == "center") {
			this.set_center_widget(widget);
		} else {
			this.pack_start(widget, !position.contains(".no-expand"), !position.contains(".no-fill"));
		}
		return true;
	}
	
	public string? command(string command, string? data) {
		string? result;
		result = LunarWidgets.CommandHelper.execute_generic_widget_command(command, data, this);
		if (result != null) { return result; }
		result = LunarWidgets.CommandHelper.execute_orientable_widget_command(command, data, this);
		if (result != null) { return result; }
		switch(command) {
			case "homogeneous":
				if (data != null) {
					this.homogeneous = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.homogeneous);
			default:
				return null;
		}
	}
	
}
