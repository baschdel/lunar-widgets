public class LunarWidgets.Widget.Grid : Gtk.Grid, LunarWidgets.Interface.Widget {
	
	public string id;
	public LunarWidgets.Interface.WidgetManager wm;
	
	public Grid(LunarWidgets.Interface.WidgetManager wm, string id) {
		this.wm = wm;
		this.id = id;
	}
	
	  ///////////////////////////////
	 // LunarWidgets.Interface.Widget //
	///////////////////////////////
	
	public string get_widget_id(){
		return this.id;
	}
	
	public bool add_widget(string position, Gtk.Widget widget){
		int x = 0, y = 0;
		int w = 1, h = 1;
		string[] tokens = position.split("_");
		if (tokens.length >= 0) {
			x = LunarWidgets.CommandHelper.parse_integer(tokens[0], 0);
		}
		if (tokens.length >= 1) {
			y = LunarWidgets.CommandHelper.parse_integer(tokens[1], 0);
		}
		if (tokens.length >= 2) {
			w = LunarWidgets.CommandHelper.parse_integer(tokens[2], 1);
		}
		if (tokens.length >= 3) {
			h = LunarWidgets.CommandHelper.parse_integer(tokens[3], 1);
		}
		this.attach(widget,x,y,w,h);
		return true;
	}
	
	public string? command(string command, string? data) {
		string? result;
		result = LunarWidgets.CommandHelper.execute_generic_widget_command(command, data, this);
		if (result != null) { return result; }
		result = LunarWidgets.CommandHelper.execute_orientable_widget_command(command, data, this);
		if (result != null) { return result; }
		switch(command) {
			case "row_homogeneous":
				if (data != null) {
					this.row_homogeneous = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.row_homogeneous);
			case "column_homogeneous":
				if (data != null) {
					this.column_homogeneous = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.column_homogeneous);
			case "row_spacing":
				if (data != null) {
					this.row_spacing = LunarWidgets.CommandHelper.parse_integer(data, 0);
				}
				return @"$(this.row_spacing)";
			case "column_spacing":
				if (data != null) {
					this.column_spacing = LunarWidgets.CommandHelper.parse_integer(data, 0);
				}
				return @"$(this.column_spacing)";
			case "baseline_row":
				if (data != null) {
					this.baseline_row = LunarWidgets.CommandHelper.parse_integer(data, 0);
				}
				return @"$(this.baseline_row)";
			case "insert_row":
				if (data != null){
					this.insert_row(LunarWidgets.CommandHelper.parse_integer(data, 0));
					return "ok";
				}
				return null;
			case "insert_column":
				if (data != null){
					this.insert_column(LunarWidgets.CommandHelper.parse_integer(data, 0));
					return "ok";
				}
				return null;
			case "remove_row":
				if (data != null){
					this.remove_row(LunarWidgets.CommandHelper.parse_integer(data, 0));
					return "ok";
				}
				return null;
			case "remove_column":
				if (data != null){
					this.remove_column(LunarWidgets.CommandHelper.parse_integer(data, 0));
					return "ok";
				}
				return null;
			default:
				return null;
		}
	}
	
}
