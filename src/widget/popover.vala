public class LunarWidgets.Widget.Popover : Gtk.Popover, LunarWidgets.Interface.Widget {
	
	public string id;
	public LunarWidgets.Interface.WidgetManager wm;
	
	public Popover(LunarWidgets.Interface.WidgetManager wm, string id) {
		this.wm = wm;
		this.id = id;
	}
	
	  ///////////////////////////////////
	 // LunarWidgets.Interface.Widget //
	///////////////////////////////////
	
	public string get_widget_id(){
		return this.id;
	}
	
	public bool add_widget(string position, Gtk.Widget widget){
		var child = this.get_child();
		if (child != null) {
			this.remove(child);
		}
		this.add(widget);
		return true;
	}
	
	public string? command(string command, string? data) {
		string? result;
		result = LunarWidgets.CommandHelper.execute_generic_widget_command(command, data, this);
		if (result != null) { return result; }
		switch(command) {
			case "popup":
				this.popup();
				return "ok";
			case "popdown":
				this.popdown();
				return "ok";
			case "position":
				if (data != null) {
					switch (data) {
						case "top":
							this.position = TOP;
							break;
						case "bottom":
							this.position = BOTTOM;
							break;
						case "left":
							this.position = LEFT;
							break;
						case "right":
							this.position = RIGHT;
							break;
						default:
							break;
					}
				}
				switch (this.position) {
					case TOP:
						return "top";
					case BOTTOM:
						return "bottom";
					case LEFT:
						return "left";
					case RIGHT:
						return "right";
					default:
						return "unknown";
				}
			case "relative_to":
				if (data != null) {
					this.set_relative_to(wm.get_widget(data));
					return "ok";
				}
				return null;
			case "modal":
 				if (data != null) {
 					this.modal = LunarWidgets.CommandHelper.string_to_bool(data);
 				}
 				return LunarWidgets.CommandHelper.bool_to_string(this.modal);
			default:
				return null;
		}
	}
	
}
