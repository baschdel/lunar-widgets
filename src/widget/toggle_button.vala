public class LunarWidgets.Widget.ToggleButton : Gtk.ToggleButton, LunarWidgets.Interface.Widget {
	
	public string id;
	public LunarWidgets.Interface.WidgetManager wm;
	
	public ToggleButton(LunarWidgets.Interface.WidgetManager wm, string id) {
		this.wm = wm;
		this.id = id;
		this.toggled.connect(on_toggled);
	}
	
	private void on_toggled() {
		wm.deliver_signal(id, "toggled", LunarWidgets.CommandHelper.bool_to_string(this.active));
	}
	
	  ///////////////////////////////////
	 // LunarWidgets.Interface.Widget //
	///////////////////////////////////
	
	public string get_widget_id(){
		return this.id;
	}
	
	public bool add_widget(string position, Gtk.Widget widget){
		this.image = widget;
		this.image.visible = true;
		return true;
	}
	
	public string? command(string command, string? data) {
		string? result;
		result = LunarWidgets.CommandHelper.execute_generic_widget_command(command, data, this);
		if (result != null) { return result; }
		switch(command) {
			case "label":
				if (data != null) {
					this.label = data;
				}
				return this.label;
			case "active":
				if (data != null) {
					this.active = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.active);
			case "inconsistent":
				if (data != null) {
					this.inconsistent = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.inconsistent);
			default:
				return null;
		}
	}
	
}
