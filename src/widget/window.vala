public class LunarWidgets.Widget.Window : Gtk.Window , LunarWidgets.Interface.Widget {
	
	public string id;
	public LunarWidgets.Interface.WidgetManager wm;
	
	public Window(LunarWidgets.Interface.WidgetManager wm, string id){
		this.wm = wm;
		this.id = id;
		this.window_position = Gtk.WindowPosition.CENTER;
		this.destroy.connect(on_destroy);
	}
	
	private void on_destroy(){
		wm.deliver_signal(id, "destroy", null);
	}
	
	  ///////////////////////////////
	 // LunarWidgets.Interface.Widget //
	///////////////////////////////
	
	public string get_widget_id(){
		return this.id;
	}
	
	public bool add_widget(string position, Gtk.Widget widget){
		this.add(widget);
		return true;
	}
	
	public string? command(string command, string? data) {
		string? result;
		result = LunarWidgets.CommandHelper.execute_generic_widget_command(command, data, this);
		if (result != null) { return result; }
		switch(command) {
			case "label":
				if (data != null) {
					this.title = data;
				}
				return this.title;
			case "accept_focus":
				if (data != null) {
					this.accept_focus = LunarWidgets.CommandHelper.string_to_bool(data);
				}
				return LunarWidgets.CommandHelper.bool_to_string(this.accept_focus);
			case "keep_above":
				if (data != null) {
					this.set_keep_above(LunarWidgets.CommandHelper.string_to_bool(data));
				}
				return "ok";
			case "keep_below":
				if (data != null) {
					this.set_keep_below(LunarWidgets.CommandHelper.string_to_bool(data));
				}
				return "ok";
			case "stick":
				this.stick();
				return "ok";
			case "unstick":
				this.unstick();
				return "ok";
			case "make_dock":
				this.type_hint = DOCK;
				return "ok";
			default:
				return null;
		}
	}
	
}
