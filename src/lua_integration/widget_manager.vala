namespace LunarWidgets.LuaIntegration.WidgetManager {
	
	/* This is a static class for noe because lua and I cuurently want this to work somehow */
	/* Also I probably will only need one of these right now*/
	
	static LunarWidgets.Interface.WidgetManager wm;
	static LunarWidgets.WidgetFactory factory;
	static unowned Lua.LuaVM luavm;
	
	/*
		Lua api:
			make_widget(string:type, string:id, [string:parent_id], [string:position]) bool
			remove_widget(string:id) bool
			unparent_widget(string:id) bool
			assign_widget(string:id, string:parent_id, [string:position]) bool
			widget_command(string:id, string:command, [string:data]) string
		Callback:
			on_widget_signal(string:id, string:signal, string:data)
	*/
	
	public static void initialize(Lua.LuaVM luavm, LunarWidgets.Interface.WidgetManager widget_manager) {
		LunarWidgets.LuaIntegration.WidgetManager.luavm = luavm;
		LunarWidgets.LuaIntegration.WidgetManager.wm = widget_manager;
		LunarWidgets.LuaIntegration.WidgetManager.factory = new LunarWidgets.WidgetFactory(wm);
		wm.deliver_signal.connect(on_widget_signal);
		luavm.register("make_widget", make_widget);
		luavm.register("remove_widget", remove_widget);
		luavm.register("unparent_widget", unparent_widget);
		luavm.register("assign_widget", assign_widget);
		luavm.register("widget_command", widget_command);
	}
	
	private static void on_widget_signal(string id, string signal_name, string? data) {
		luavm.get_global("on_widget_signal");
		if (luavm.is_nil(-1)) {
			luavm.pop(1);
			return;
		}
		luavm.push_string(id);
		luavm.push_string(signal_name);
		if (data != null) {
			luavm.push_string(data);
		} else {
			luavm.push_nil();
		}
		var res = luavm.pcall(3,0,0); //3 arg, 0 ret, original error object
		if (res != 0) {
			print("[Lua][ERROR][on_widget_signal()] "+luavm.to_string(1)+"\n");
			luavm.pop(1);
		}
	}
	
	private static int make_widget(Lua.LuaVM luavm) {
		if (!luavm.is_string(1)) {
			luavm.push_nil();
			luavm.push_string("make_widget(): type: string expected");
			return 2;
		}
		if (!luavm.is_string(2)) {
			luavm.push_nil();
			luavm.push_string("make_widget(): id: string expected");
			return 2;
		}
		if (!(luavm.is_string(3) || luavm.is_none_or_nil(3))) {
			luavm.push_nil();
			luavm.push_string("make_widget(): parent_id: string or nil expected");
			return 2;
		}
		if (!(luavm.is_string(4) || luavm.is_none_or_nil(4))) {
			luavm.push_nil();
			luavm.push_string("make_widget(): position: string or nil expected");
			return 2;
		}
		string parent_id = "nil";
		if (luavm.is_string(3)) {
			parent_id = luavm.to_string(3);
		}
		string position = "";
		if (luavm.is_string(4)) {
			position = luavm.to_string(4);
		}
		luavm.push_boolean(factory.produce(luavm.to_string(1), luavm.to_string(2), parent_id, position)? 1:0);
		return 1;
	}
	
	private static int remove_widget(Lua.LuaVM luavm) {
		if (!luavm.is_string(1)) {
			luavm.push_nil();
			luavm.push_string("remove_widget(): id: string expected");
			return 2;
		}
		luavm.push_boolean(wm.remove_widget(luavm.to_string(1))? 1:0);
		return 1;
	}
	
	private static int unparent_widget(Lua.LuaVM luavm) {
		if (!luavm.is_string(1)) {
			luavm.push_nil();
			luavm.push_string("unparent_widget(): id: string expected");
			return 2;
		}
		luavm.push_boolean(wm.unparent_widget(luavm.to_string(1))? 1:0);
		return 1;
	}
	
	private static int assign_widget(Lua.LuaVM luavm) {
		if (!luavm.is_string(1)) {
			luavm.push_nil();
			luavm.push_string("assign_widget(): id: string expected");
			return 2;
		}
		if (!luavm.is_string(2)) {
			luavm.push_nil();
			luavm.push_string("assign_widget(): parent_id: string expected");
			return 2;
		}
		if (!(luavm.is_string(3) || luavm.is_none_or_nil(3))) {
			luavm.push_nil();
			luavm.push_string("assign_widget(): position: string or nil expected");
			return 2;
		}
		luavm.push_boolean(wm.assign_widget(luavm.to_string(1), luavm.to_string(2), luavm.to_string(3))? 1:0);
		return 1;
	}
	
	private static int widget_command(Lua.LuaVM luavm) {
		if (!luavm.is_string(1)) {
			luavm.push_nil();
			luavm.push_string("widget_command(): id: string expected");
			return 2;
		}
		if (!luavm.is_string(2)) {	
			luavm.push_nil();
			luavm.push_string("widget_command(): command: string expected");
			return 2;
		}
		if (!(luavm.is_string(3) || luavm.is_none_or_nil(3))) {
			luavm.push_nil();
			luavm.push_string("widget_command(): data: string or nil expected");
			return 2;
		}
		string? data = null;
		if (luavm.is_string(3)) {
			data = luavm.to_string(3);
		}
		string? result = wm.try_execute_command(luavm.to_string(1), luavm.to_string(2), data);
		if (result == null) {
			luavm.push_nil();
		} else {
			luavm.push_string(result);
		}
		return 1;
	}
	
}
